//Ryan Hartsock
//10/19/2013
//Project 2

import java.util.Random;

public class ArrayGenerator {
	// Ordered Arrays
	int[] ordered100 = new int[100];
	int[] ordered1000 = new int[1000];
	int[] ordered10000 = new int[10000];

	// Random Arrays
	int[] random100 = new int[100];
	int[] random1000 = new int[1000];
	int[] random10000 = new int[10000];

	// Semi-Ordered Arrays
	int[] semiOrdered100 = new int[100];
	int[] semiOrdered1000 = new int[1000];
	int[] semiOrdered10000 = new int[10000];

	public void arrayGenerator() {

	}

	// Generate the ordered arrays
	public void generateOrdered100() {
		for (int i = 0; i < 100; i++) {
			ordered100[i] = i;
		}
	}

	public void generateOrdered1000() {
		for (int i = 0; i < 1000; i++) {
			ordered1000[i] = i;
		}
	}

	public void generateOrdered10000() {
		for (int i = 0; i < 10000; i++) {
			int c = 10000;
			ordered10000[i] = c;
			c--;
		}
	}

	// Generate the random arrays
	public void generateRandom100() {
		Random generator = new Random();
		for (int i = 0; i < 100; i++) {
			int r = generator.nextInt();
			random100[i] = r;
		}
	}

	public void generateRandom1000() {
		Random generator = new Random();
		for (int i = 0; i < 1000; i++) {
			int r = generator.nextInt();
			random1000[i] = r;
		}
	}

	public void generateRandom10000() {
		Random generator = new Random();
		for (int i = 0; i < 10000; i++) {
			int r = generator.nextInt();
			random10000[i] = r;
		}
	}

	// Generate the semi-ordered arrays
	public void generateSemiOrdered100() {
		Random generator = new Random();
		for (int i = 0; i < 100; i++) {
			semiOrdered100[i] = i;
		}
		for (int i = 0; i < 100; i += 10) {
			int r = generator.nextInt();
			semiOrdered100[i] = r;
		}
	}

	public void generateSemiOrdered1000() {
		Random generator = new Random();
		for (int i = 0; i < 1000; i++) {
			semiOrdered1000[i] = i;
		}
		for (int i = 0; i < 1000; i += 10) {
			int r = generator.nextInt();
			semiOrdered1000[i] = r;
		}
	}

	public void generateSemiOrdered10000() {
		Random generator = new Random();
		for (int i = 0; i < 10000; i++) {
			semiOrdered10000[i] = i;
		}
		for (int i = 0; i < 10000; i += 10) {
			int r = generator.nextInt();
			semiOrdered10000[i] = r;
		}
	}

}
