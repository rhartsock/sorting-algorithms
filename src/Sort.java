//Ryan Hartsock
//10/19/2013
//Project 2

import java.util.*;

public class Sort {
	public static void main(String[] args) {

		// Generate the 6 arrays
		ArrayGenerator array1 = new ArrayGenerator();
		ArrayGenerator array2 = new ArrayGenerator();
		ArrayGenerator array3 = new ArrayGenerator();
		ArrayGenerator array4 = new ArrayGenerator();
		ArrayGenerator array5 = new ArrayGenerator();
		ArrayGenerator array6 = new ArrayGenerator();

		// Run the algorithms for ordered
		System.out.println("Ordered");
		generateOrdered(array1, array2, array3, array4, array5, array6);
		ordered100(array1, array2, array3, array4, array5, array6);
		ordered1000(array1, array2, array3, array4, array5, array6);
		ordered10000(array1, array2, array3, array4, array5, array6);

		// Run the algorithms for random
		System.out.println("");
		System.out.println("");
		System.out.println("Random");
		generateRandom(array1, array2, array3, array4, array5, array6);
		random100(array1, array2, array3, array4, array5, array6);
		random1000(array1, array2, array3, array4, array5, array6);
		random10000(array1, array2, array3, array4, array5, array6);

		// Run the algorithms for semi-ordered
		System.out.println("");
		System.out.println("Semi Ordered");
		generateSemiOrdered(array1, array2, array3, array4, array5, array6);
		semiOrdered100(array1, array2, array3, array4, array5, array6);
		semiOrdered1000(array1, array2, array3, array4, array5, array6);
		semiOrdered10000(array1, array2, array3, array4, array5, array6);
	}

	// Generation Functions For Random Arrays
	private static void generateRandom(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		array1.generateRandom100();
		array1.generateRandom1000();
		array1.generateRandom10000();

		array2.generateRandom100();
		array2.generateRandom1000();
		array2.generateRandom10000();

		array3.generateRandom100();
		array3.generateRandom1000();
		array3.generateRandom10000();

		array4.generateRandom100();
		array4.generateRandom1000();
		array4.generateRandom10000();

		array5.generateRandom100();
		array5.generateRandom1000();
		array5.generateRandom10000();

		array6.generateRandom100();
		array6.generateRandom1000();
		array6.generateRandom10000();
	}

	// Generation Functions For Ordered Arrays
	private static void generateOrdered(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		array1.generateOrdered100();
		array1.generateOrdered1000();
		array1.generateOrdered10000();

		array2.generateOrdered100();
		array2.generateOrdered1000();
		array2.generateOrdered10000();

		array3.generateOrdered100();
		array3.generateOrdered1000();
		array3.generateOrdered10000();

		array4.generateOrdered100();
		array4.generateOrdered1000();
		array4.generateOrdered10000();

		array5.generateOrdered100();
		array5.generateOrdered1000();
		array5.generateOrdered10000();

		array6.generateOrdered100();
		array6.generateOrdered1000();
		array6.generateOrdered10000();
	}

	// Generation Functions For Semi-Ordered Arrays
	private static void generateSemiOrdered(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		array1.generateSemiOrdered100();
		array1.generateSemiOrdered1000();
		array1.generateSemiOrdered10000();

		array2.generateSemiOrdered100();
		array2.generateSemiOrdered1000();
		array2.generateSemiOrdered10000();

		array3.generateSemiOrdered100();
		array3.generateSemiOrdered1000();
		array3.generateSemiOrdered10000();

		array4.generateSemiOrdered100();
		array4.generateSemiOrdered1000();
		array4.generateSemiOrdered10000();

		array5.generateSemiOrdered100();
		array5.generateSemiOrdered1000();
		array5.generateSemiOrdered10000();

		array6.generateSemiOrdered100();
		array6.generateSemiOrdered1000();
		array6.generateSemiOrdered10000();
	}

	// Execution Functions Ordered 100
	private static void ordered100(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 100
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.ordered100);
		long end1 = new Date().getTime();
		System.out.print("100 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 100
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.ordered100);
		long end4 = new Date().getTime();
		System.out.print("100 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 100
		long start2 = new Date().getTime();
		insertionSort(array2.ordered100);
		long end2 = new Date().getTime();
		System.out.print("100 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 100
		long start3 = new Date().getTime();
		selectionSort(array3.ordered100);
		long end3 = new Date().getTime();
		System.out.print("100 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 100
		long start6 = new Date().getTime();
		mergeSort(array6.ordered100);
		long end6 = new Date().getTime();
		System.out.print("100 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 100
		long start5 = new Date().getTime();
		quickSort(array5.ordered100);
		long end5 = new Date().getTime();
		System.out.print("100 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Ordered 1000
	private static void ordered1000(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 1,000
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.ordered1000);
		long end1 = new Date().getTime();
		System.out.print("1,000 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 1,000
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.ordered1000);
		long end4 = new Date().getTime();
		System.out.print("1,000 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 1,000
		long start2 = new Date().getTime();
		insertionSort(array2.ordered1000);
		long end2 = new Date().getTime();
		System.out.print("1,000 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 1,000
		long start3 = new Date().getTime();
		selectionSort(array3.ordered1000);
		long end3 = new Date().getTime();
		System.out.print("1,000 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 1,000
		long start6 = new Date().getTime();
		mergeSort(array6.ordered1000);
		long end6 = new Date().getTime();
		System.out.print("1,000 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 1,000
		long start5 = new Date().getTime();
		quickSort(array5.ordered1000);
		long end5 = new Date().getTime();
		System.out.print("1,000 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Ordered 10000
	private static void ordered10000(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 10,000
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.ordered10000);
		long end1 = new Date().getTime();
		System.out.print("10,000 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 10,000
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.ordered10000);
		long end4 = new Date().getTime();
		System.out.print("10,000 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 10,000
		long start2 = new Date().getTime();
		insertionSort(array2.ordered10000);
		long end2 = new Date().getTime();
		System.out.print("10,000 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 10,000
		long start3 = new Date().getTime();
		selectionSort(array3.ordered10000);
		long end3 = new Date().getTime();
		System.out.print("10,000 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 10,000
		long start6 = new Date().getTime();
		mergeSort(array6.ordered10000);
		long end6 = new Date().getTime();
		System.out.print("10,000 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 10,000
		long start5 = new Date().getTime();
		quickSort(array5.ordered10000);
		long end5 = new Date().getTime();
		System.out.print("10,000 Quick sort: N/A");
		System.out.println(end5 - start5);
	}

	// Execution Functions Random 100
	private static void random100(ArrayGenerator array1, ArrayGenerator array2,
			ArrayGenerator array3, ArrayGenerator array4,
			ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 100
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.random100);
		long end1 = new Date().getTime();
		System.out.print("100 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 100
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.random100);
		long end4 = new Date().getTime();
		System.out.print("100 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 100
		long start2 = new Date().getTime();
		insertionSort(array2.random100);
		long end2 = new Date().getTime();
		System.out.print("100 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 100
		long start3 = new Date().getTime();
		selectionSort(array3.random100);
		long end3 = new Date().getTime();
		System.out.print("100 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 100
		long start6 = new Date().getTime();
		mergeSort(array6.random100);
		long end6 = new Date().getTime();
		System.out.print("100 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 100
		long start5 = new Date().getTime();
		quickSort(array5.random100);
		long end5 = new Date().getTime();
		System.out.print("100 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Random 1000
	private static void random1000(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 1000
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.random1000);
		long end1 = new Date().getTime();
		System.out.print("1000 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 1000
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.random1000);
		long end4 = new Date().getTime();
		System.out.print("1000 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 1000
		long start2 = new Date().getTime();
		insertionSort(array2.random1000);
		long end2 = new Date().getTime();
		System.out.print("1000 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 1000
		long start3 = new Date().getTime();
		selectionSort(array3.random1000);
		long end3 = new Date().getTime();
		System.out.print("1000 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 1000
		long start6 = new Date().getTime();
		mergeSort(array6.random1000);
		long end6 = new Date().getTime();
		System.out.print("1000 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 1000
		long start5 = new Date().getTime();
		quickSort(array5.random1000);
		long end5 = new Date().getTime();
		System.out.print("1000 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Random 10000
	private static void random10000(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 10000
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.random10000);
		long end1 = new Date().getTime();
		System.out.print("10000 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 10000
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.random10000);
		long end4 = new Date().getTime();
		System.out.print("10000 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 10000
		long start2 = new Date().getTime();
		insertionSort(array2.random10000);
		long end2 = new Date().getTime();
		System.out.print("10000 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 10000
		long start3 = new Date().getTime();
		selectionSort(array3.random10000);
		long end3 = new Date().getTime();
		System.out.print("10000 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 10000
		long start6 = new Date().getTime();
		mergeSort(array6.random10000);
		long end6 = new Date().getTime();
		System.out.print("10000 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 10000
		long start5 = new Date().getTime();
		quickSort(array5.random10000);
		long end5 = new Date().getTime();
		System.out.print("10000 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Semi-Ordered 100
	private static void semiOrdered100(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 100
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.semiOrdered100);
		long end1 = new Date().getTime();
		System.out.print("100 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 100
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.semiOrdered100);
		long end4 = new Date().getTime();
		System.out.print("100 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 100
		long start2 = new Date().getTime();
		insertionSort(array2.semiOrdered100);
		long end2 = new Date().getTime();
		System.out.print("100 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 100
		long start3 = new Date().getTime();
		selectionSort(array3.semiOrdered100);
		long end3 = new Date().getTime();
		System.out.print("100 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 100
		long start6 = new Date().getTime();
		mergeSort(array6.semiOrdered100);
		long end6 = new Date().getTime();
		System.out.print("100 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 100
		long start5 = new Date().getTime();
		quickSort(array5.semiOrdered100);
		long end5 = new Date().getTime();
		System.out.print("100 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Semi-Ordered 1000
	private static void semiOrdered1000(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 1000
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.semiOrdered1000);
		long end1 = new Date().getTime();
		System.out.print("1000 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 1000
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.semiOrdered1000);
		long end4 = new Date().getTime();
		System.out.print("1000 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 1000
		long start2 = new Date().getTime();
		insertionSort(array2.semiOrdered1000);
		long end2 = new Date().getTime();
		System.out.print("1000 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 1000
		long start3 = new Date().getTime();
		selectionSort(array3.semiOrdered1000);
		long end3 = new Date().getTime();
		System.out.print("1000 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 1000
		long start6 = new Date().getTime();
		mergeSort(array6.semiOrdered1000);
		long end6 = new Date().getTime();
		System.out.print("1000 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 1000
		long start5 = new Date().getTime();
		quickSort(array5.semiOrdered1000);
		long end5 = new Date().getTime();
		System.out.print("1000 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Execution Functions Semi-Ordered 10000
	private static void semiOrdered10000(ArrayGenerator array1,
			ArrayGenerator array2, ArrayGenerator array3,
			ArrayGenerator array4, ArrayGenerator array5, ArrayGenerator array6) {

		// Bubble WO 10000
		long start1 = new Date().getTime();
		bubbleSortWithOutSwaps(array1.semiOrdered10000);
		long end1 = new Date().getTime();
		System.out.print("10000 Bubble sort without swaps: ");
		System.out.println(end1 - start1);

		// Bubble W 10000
		long start4 = new Date().getTime();
		bubbleSortWithSwaps(array4.semiOrdered10000);
		long end4 = new Date().getTime();
		System.out.print("10000 Bubble sort with swaps: ");
		System.out.println(end4 - start4);

		// Insertion 10000
		long start2 = new Date().getTime();
		insertionSort(array2.semiOrdered10000);
		long end2 = new Date().getTime();
		System.out.print("10000 Insertion sort: ");
		System.out.println(end2 - start2);

		// Selection 10000
		long start3 = new Date().getTime();
		selectionSort(array3.semiOrdered10000);
		long end3 = new Date().getTime();
		System.out.print("10000 Selection sort: ");
		System.out.println(end3 - start3);

		// Merge 10000
		long start6 = new Date().getTime();
		mergeSort(array6.semiOrdered10000);
		long end6 = new Date().getTime();
		System.out.print("10000 Merge sort: ");
		System.out.println(end6 - start6);

		// Quick 10000
		long start5 = new Date().getTime();
		quickSort(array5.semiOrdered10000);
		long end5 = new Date().getTime();
		System.out.print("10000 Quick sort: ");
		System.out.println(end5 - start5);
	}

	// Insertion Sort
	public static void insertionSort(int[] array) {
		for (int i = 0; i < array.length; i++) {
			int key = array[i];
			int j = i - 1;
			while (j >= 0 && array[j] > key) {
				array[j + 1] = array[j];
				j = j - 1;
			}
			array[j + 1] = key;
		}
	}

	// Bubble Sort Without Swaps
	public static void bubbleSortWithOutSwaps(int[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = array.length; j > i + 1; j--) {
				if (array[j - 1] < array[j - 2]) {
					int temp1 = 0;
					int temp2 = 0;
					temp1 = array[j - 1];
					temp2 = array[j - 2];
					array[j - 2] = temp1;
					array[j - 1] = temp2;
				}
			}
		}
	}

	// Bubble Sort With Swaps
	public static void bubbleSortWithSwaps(int[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			int swaps = 0;
			for (int j = array.length; j > i + 1; j--) {
				if (array[j - 1] < array[j - 2]) {
					int temp1 = 0;
					int temp2 = 0;
					temp1 = array[j - 1];
					temp2 = array[j - 2];
					array[j - 2] = temp1;
					array[j - 1] = temp2;
					swaps += 1;
				}
			}
			if (swaps == 0) {
				break;
			}
		}
	}

	// Selection Sort
	public static void selectionSort(int[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			int key = i;
			for (int j = i + 1; j < array.length; j++) {
				if (array[key] > array[j]) {
					key = j;
				}
			}
			if (key != i) {
				int temp1 = 0;
				int temp2 = 0;
				temp1 = array[i];
				temp2 = array[key];
				array[key] = temp1;
				array[i] = temp2;
			}
		}
	}

	// Quick Sort
	public static void quickSort(int[] array) {
		quickSort(array, 0, array.length - 1);
	}

	// Quick Sort
	public static void quickSort(int[] array, int p, int r) {
		if (p < r) {
			int pivotIndex = partition(array, p, r);
			quickSort(array, p, pivotIndex - 1);
			quickSort(array, pivotIndex + 1, r);
		}
	}

	// Partition
	public static int partition(int[] array, int p, int r) {
		int pivot = array[p];
		int low = p + 1;
		int high = r;

		while (high > low) {
			while (low <= high && array[low] <= pivot) {
				low++;
			}
			while (low <= high && array[high] > pivot) {
				high--;
			}
			if (high > low) {
				int temp1 = array[high];
				array[high] = array[low];
				array[low] = temp1;
			}
		}
		while (high > p && array[high] >= pivot) {
			high--;
		}
		if (pivot > array[high]) {
			array[p] = array[high];
			array[high] = pivot;
			return high;
		} else {
			return p;
		}
	}

	// Merge Sort
	public static void mergeSort(int[] array) {
		if (array.length > 1) {
			int[] first = new int[array.length / 2];
			System.arraycopy(array, 0, first, 0, array.length / 2);
			mergeSort(first);

			int secondLength = array.length - array.length / 2;
			int[] second = new int[secondLength];
			System.arraycopy(array, array.length / 2, second, 0, secondLength);
			mergeSort(second);

			merge(first, second, array);
		}
	}

	// Merge
	public static void merge(int[] array1, int[] array2, int[] temp) {
		int index1 = 0;
		int index2 = 0;
		int index3 = 0;

		while (index1 < array1.length && index2 < array2.length) {
			if (array1[index1] < array2[index2]) {
				temp[index3++] = array1[index1++];
			} else {
				temp[index3++] = array2[index2++];
			}
		}
		while (index1 < array1.length) {
			temp[index3++] = array1[index1++];
		}
		while (index2 < array2.length) {
			temp[index3++] = array2[index2++];
		}
	}

}
